<%-- 
    Document   : index
    Created on : 07-05-2021, 3:22:52
    Author     : camilo williams
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Camilo Williams Seccion 50</h1>
        <b>URL GET: http://desktop-l4es1k7:8081/Ev3apirest-1.0-SNAPSHOT/api/usuario</b><br><br>
        <b>URL PUT: http://desktop-l4es1k7:8081/Ev3apirest-1.0-SNAPSHOT/api/usuario</b><br><br>
        <b>URL POST: http://desktop-l4es1k7:8081/Ev3apirest-1.0-SNAPSHOT/api/usuario</b><br><br>
        <b>URL DELETE: http://desktop-l4es1k7:8081/Ev3apirest-1.0-SNAPSHOT/api/usuario/{deleteid}</b><br><br>
    </body>
</html>
