/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ev3apirest.dao;

import com.mycompany.ev3apirest.dao.exceptions.NonexistentEntityException;
import com.mycompany.ev3apirest.dao.exceptions.PreexistingEntityException;
import com.mycompany.ev3apirest.entity.Creacionusuario;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author camilo williams
 */
public class CreacionusuarioJpaController implements Serializable {

    public CreacionusuarioJpaController() {
       
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Creacionusuario creacionusuario) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(creacionusuario);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCreacionusuario(creacionusuario.getRut()) != null) {
                throw new PreexistingEntityException("Creacionusuario " + creacionusuario + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Creacionusuario creacionusuario) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            creacionusuario = em.merge(creacionusuario);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = creacionusuario.getRut();
                if (findCreacionusuario(id) == null) {
                    throw new NonexistentEntityException("The creacionusuario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Creacionusuario creacionusuario;
            try {
                creacionusuario = em.getReference(Creacionusuario.class, id);
                creacionusuario.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The creacionusuario with id " + id + " no longer exists.", enfe);
            }
            em.remove(creacionusuario);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Creacionusuario> findCreacionusuarioEntities() {
        return findCreacionusuarioEntities(true, -1, -1);
    }

    public List<Creacionusuario> findCreacionusuarioEntities(int maxResults, int firstResult) {
        return findCreacionusuarioEntities(false, maxResults, firstResult);
    }

    private List<Creacionusuario> findCreacionusuarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Creacionusuario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Creacionusuario findCreacionusuario(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Creacionusuario.class, id);
        } finally {
            em.close();
        }
    }

    public int getCreacionusuarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Creacionusuario> rt = cq.from(Creacionusuario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
