/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ev3apirest;

import com.mycompany.ev3apirest.dao.CreacionusuarioJpaController;
import com.mycompany.ev3apirest.dao.exceptions.NonexistentEntityException;
import com.mycompany.ev3apirest.entity.Creacionusuario;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author camilo williams
 */
@Path("usuario")
public class CreacionUsuario {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarusuarios(){
    
    CreacionusuarioJpaController dao=new CreacionusuarioJpaController();
    List<Creacionusuario> lista=dao.findCreacionusuarioEntities();
    
    
    return Response.ok(200).entity(lista).build();
    
    
    }
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response adduser(Creacionusuario crearuser){
    
        try {
            CreacionusuarioJpaController dao=new CreacionusuarioJpaController();
            dao.create(crearuser);
        } catch (Exception ex) {
            Logger.getLogger(CreacionUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(crearuser).build();
    }    
    
    @DELETE
    @Path("/{deleteid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deluser(@PathParam("deleteid") String deleteid){
    
        try {
            CreacionusuarioJpaController dao=new CreacionusuarioJpaController();
            dao.destroy(deleteid);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(CreacionUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok("usuario eliminado").build();
       
    }   
    
    @PUT
    public Response update(Creacionusuario crearuser){
    
        try {
            CreacionusuarioJpaController dao=new CreacionusuarioJpaController();
            dao.edit(crearuser);
        } catch (Exception ex) {
            Logger.getLogger(CreacionUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(crearuser).build();
    
    }
    
}
